let id=Math.floor(Date.now() / 1000);//while editing the pages id was causing issue since the same was duplicating.
export function dropComponent (droppedComponent,data,parent) {
  return {
    type: 'DROP_COMPONENT',
    id: ++id,
    data,
    droppedComponent,
    parent: parent == undefined ? "" : parent
  };
}

export function removeComponent (task) {
  return {
    type: 'REMOVE_COMPONENT',
    task
  };
}

export function SetCurrentComponent (droppedComponent,data) {
  return {
    type: 'SetCurrentComponent',
    droppedComponent,
    data
  };
}

export function setProperties (currentComponent,data) {
  return {
    type: 'SET_PROPERTIES',
    currentComponent,
    data
  }
}

export function setEdit (currentComponent = '') {
  return {
    type: 'SET_EDIT',
    currentComponent
  }
}

export function onChangingPageUrl (newUrl) {
  return {
    type: 'onChangingPageUrl',
    newUrl
  }
}

export function removeComponent (currentComponent) {
  return {
    type: 'REMOVE_COMPONENT',
    currentComponent
  }
}

function receivePosts(json) {
  return {
    type: 'RECEIVE_POSTS',
    posts: json
  }
}

export function fetchPosts(field,value) {
  return dispatch => {
    fetch('http://localhost:1337/name?'+field+'='+value).then((response) => {
      return response.json();
    }).then((json) => {
      if(field==='id'){
        dispatch(receivePosts(json.data));
        dispatch(setFinalFalse());
      }else{
      dispatch(receivePosts(json[0].data));        
      }
    }).catch((ex) => {
      console.log('parsing failed', ex);
    })
  }
}
export function clearState() {
  return {
    type: 'CLEARSTATE'
  }
}

function setFinal() {
  return {
    type: 'SET_FINAL'
  }
}

function setFinalFalse() {
  return {
    type: 'SET_FINAL_FALSE'
  }
}

export function sendPosts(pageUrl,selectedComponents) {
  
  return dispatch => {
    dispatch(setFinal());
      fetch('http://localhost:1337/name', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        pageUrl:pageUrl,
        data:selectedComponents
      })
    }).then((response) => {
      dispatch(clearState()); 
    }).catch((ex) => {
      console.log('parsing failed', ex);
    });
  }
}
export function closeModal() {
  return {
    type: 'CLOSEMODAL'
  }
}
export function openModal() {
  return {
    type: 'OPENMODAL',
  }
}
export function saveLS(currentComponent, layouts) {
  console.log("current ", currentComponent);
  return {
    type: 'SAVE_LS',
    layouts,
    currentComponent
  }
}
export function updateItem(component,itemkey,updateditemkey,updatedoptions,intialState){
  return {
    type: 'EDITITEM',
    component,
    itemkey,
    updateditemkey,
    updatedoptions,
    intialState
  }
}
export function updatePage(id,pageUrl,selectedComponents) {
  console.log(id,pageUrl,selectedComponents);
  return dispatch => {
    dispatch(setFinal());
      fetch('http://localhost:1337/name/'+id, {
      method: 'put',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        pageUrl:pageUrl,
        data:selectedComponents
      })
    }).then((response) => {
      dispatch(clearState()); 
    }).catch((ex) => {
      console.log('parsing failed', ex);
    });
  }
}