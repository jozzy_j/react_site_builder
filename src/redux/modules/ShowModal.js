export default (state =false, action) => {
  switch (action.type) {
    case 'CLOSEMODAL':
      return false;
    case 'OPENMODAL':
      return true;
    default:
      return state;
  }
};