export default (state = {}, action) => {
  switch (action.type) {
    case 'DROP_COMPONENT':
      return Object.assign({},state,{
              [action.droppedComponent+'_'+action.id]:{
                edit: true,
                final: false,
                parent: action.parent,
                data: action.data,
              }
            });
    case 'SET_EDIT':
    	return setEditFlag(state,action.currentComponent);
    case 'REMOVE_COMPONENT':
      return deleteComponent(state,action.currentComponent);
     case 'SET_PROPERTIES':
      return setPropertiesCurrentComponent(state,action.currentComponent,action.data);
    case 'RECEIVE_POSTS':
      return Object.assign({},state,action.posts);
    case 'CLEARSTATE':
      return {};
    case 'SET_FINAL':
      return setFinal(state);  
    case 'SET_FINAL_FALSE':
      return setFinalFalse(state);
    case 'SAVE_LS':
      return saveLayouts(state, action.currentComponent, action.layouts);
    case 'EDITITEM':
      return editItem(state,action.component,action.itemkey,action.updateditemkey,action.updatedoptions,action.intialState);
    default:
      return state;
  }
};

let setFinal = (state) => {
  let components = Object.assign({},state);
  Object.keys(components).forEach((key) => {
    components[key].final = true;
  });
  return components;
}

let setFinalFalse = (state) => {
  let components = Object.assign({},state);
  Object.keys(components).forEach((key) => {
    components[key].final = false;
  });
  return components;
}

let deleteComponent = (state, currentComponent) => {
  let components = Object.assign({},state);
  delete components[currentComponent];
  return components;
}

let setPropertiesCurrentComponent = (state, currentComponent, data) => {
	let components = Object.assign({},state);
  switch (currentComponent.split('_')[0]) {
    case 'NavBar'://complex elements propertires handled
    let properties={};
    let options=data.properties.options.split(',');
    let navBarItems=components[currentComponent].data.properties.listItems;
    if(navBarItems==''){
      navBarItems=[];
    }
      let copiedListItems = navBarItems.map((obj,i)=>{
       let rObj = {};
       rObj['title']=obj['title'];
       rObj['options']=obj['options'];
       return rObj;
      });
      copiedListItems.push({title:data.properties.title,options:options});
    console.log(copiedListItems);

    //navBarItems.push({title:data.properties.title,options:options});
    let listItems=Object.assign({},copiedListItems,{
              [data.properties.title]:
                options
            });
    let navBarinitialState=Object.assign({},initialState,data.initialState);
    properties=Object.assign({},properties,{properties:{listItems:copiedListItems,initialState:navBarinitialState}})
    data=properties;
    break;
    case 'CarouselInstance':
    let property={};
    let Items=components[currentComponent].data.properties.items;
    if(Items==''){
      Items=[];
    }
    let src;
    if(data.properties.file==''){
      src='';
    }else{
      src=data.properties.file.split(/\\/)[2].split(/\./)[0];
    }
    let input=Object.assign({},input,{file:src,contentTitle:data.properties.contentTitle,content:data.properties.content});
    Items.push(input);
    let initialState=Object.assign({},initialState,data.initialState);
    property=Object.assign({},property,{properties:{items:Items,initialState:initialState}});
    data=property;
    break;
    case 'Header':
    let HeaderInitialState=Object.assign({},HeaderInitialState,data.initialState);
    let Headerproperty=Object.assign({},Headerproperty,{properties:{content:data.properties.content,fontSize:data.properties.fontSize,color:data.properties.color,initialState:HeaderInitialState}});
    data=Headerproperty;
    console.log(Headerproperty); 
    break;
    default:
    data;
  }
	components[currentComponent].data = data;
	return components;
}

let setEditFlag = (state, currentComponent) => {
	let components = Object.assign({},state);
	for(var key in components)
	{
		if(key == currentComponent)	{
			components[key].edit=true;
		}
		if(key != currentComponent || currentComponent == '')
		{
			components[key].edit=false;
		}
	}
	return components;
}

let saveLayouts = (state, currentComponent, layouts) => {
  let components = Object.assign({},state);
  let keyToSet = '';
  Object.keys(components).forEach((key) => {
    if (key.split('_')[0] === currentComponent) {
      keyToSet = key;
    }
  })
  components[keyToSet].data.layouts = layouts;
  return components;
}

let editItem = (state,component,index,updateditemkey,updatedoptions,intialState)  => {
  let components = Object.assign({},state);
  let listItem=components[component].data.properties.listItems;
  let copy={};
    let options=updatedoptions.split(',');

  let formattedListItems = listItem.map((obj,i)=>{
   if(index==i){
       var rObj = {};
       rObj['title']=updateditemkey;
       rObj['options']=options;
       return rObj;
   }else{
    return obj;
   }
});
    let listItems=Object.assign({},formattedListItems);
    let navBarinitialState=Object.assign({},navBarinitialState,intialState);
    let properties=Object.assign({},properties,{properties:{listItems:formattedListItems,initialState:navBarinitialState}});
    components[component].data = properties;
  return components;
}
