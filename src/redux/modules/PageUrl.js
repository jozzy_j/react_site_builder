export default (state = '', action) => {
  switch (action.type) {
    case 'onChangingPageUrl':
      return action.newUrl;
    case 'CLEARSTATE':
      return '';
    default:
      return state;
  }
};
