import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import counter from './modules/counter';
import selectedComponents from './modules/SelectedComponent';
import pageUrl from './modules/PageUrl';
import showModal from './modules/ShowModal';
const page = combineReducers({
  selectedComponents,
  pageUrl
});
export default combineReducers({
  counter,
  router,
  page,
  showModal
});

