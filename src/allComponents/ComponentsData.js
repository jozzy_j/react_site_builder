
import deepfreeze from 'deep-freeze';

const ComponentData = {
  PageHeader: {
    properties: {
      file: 'sigmaImg'
  	}
  },
  ItemDescription: {
  	properties: {
  	  file: 'defaultImage',
  	  itemName: 'sigma',
  	  itemDetails: 'description'
  	}
  },
  PageFooter: {
    properties: {
      text: 'Sigma-Aldrich@2016'
    }
  },
  ProductPanelContainer: {
    properties: {
      title: ['title1','title2','title3','title4','title4','title6'],
      tabsCount: 6
    }
  },
GridLayout:{
    properties:{
    },
    layouts:{
    }
  },
  VerticalLayout: {
    properties: {}
  },
  HorizontalLayout: {
    properties: {}
  },
  HomePageTemplate: {
    properties: {}
  },
  CarouselInstance: {
    properties: {
      initialState: {
      file: 'defaultImage',
      contentTitle: 'Title of slide',
      content: 'Slide content'},
      items: [],
      direction: 'left'
    }
  },
  NavBar: {
    properties: {
    initialState: {
      title: 'title',
      options: 'Options'},
      listItems: [{title:'Title',options:['Options1','Options2','Options3']}]
    }
  },
  CustomLink: {
    properties: {
      text: 'Add text here',
      linkValue: 'Add the target of link here'
    }
  },
  CustomImage: {
    properties: {
      file: 'defaultImage',
      name: 'Image',
      linkValue: '#'
    }
  },
  Paragraph: {
    properties: {
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet nemo harum voluptas aliquid rem possimus nostrum excepturi!'
    }
  },
  Header: {
    properties: {
      content: 'Header',
      fontSize:'small',
      color:'red',
      initialState: {
        content: 'Header',
        fontSize:['small','medium','large','larger'],
        color:['red','black','blue']
      }
    }
  },
  InfoPageTemplate: {
    properties: {
    }
  }
};



deepfreeze(ComponentData);
export default ComponentData