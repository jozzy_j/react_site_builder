import React, { PropTypes} from 'react'
export class GenericWrapper extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.array.isRequired
  };
constructor (props) {
  super(props)
}
  render () {
    return (
    <div className='container '><h1>{this.props.title}</h1>{this.props.children}</div>
  )
  }
}

export default GenericWrapper