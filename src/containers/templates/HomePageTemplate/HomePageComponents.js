export ProductPanelContainer from 'components/componentsToPick/ProductPanelContainer/ProductPanelContainer';
export CarouselInstance from 'components/componentsToPick/Carousel';
export NavBar from 'components/componentsToPick/NavBar';
export PageHeader from 'components/componentsToPick/PageHeader';
export PageFooter from 'components/componentsToPick/PageFooter';
