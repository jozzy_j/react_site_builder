import React from 'react';
import * as components from './HomePageComponents';
import ComponentsData from 'allComponents/ComponentsData';
import { connect } from 'react-redux';
import {setEdit, removeComponent, dropComponent} from 'redux/actions';
import DroppableWrapper from 'containers/wrappers/DroppableWrapper';
import EditableWrapper from 'containers/wrappers/EditableWrapper';

var final = '';
class HomePageTemplate extends React.Component {
  static propTypes = {
    dispatch: React.PropTypes.func,
    selectedComponents: React.PropTypes.object
  };
  handleDelete = (currrentKey) => {
    let { selectedComponents, dispatch } = this.props;
    Object.keys(selectedComponents).forEach((key) => {
      if (selectedComponents[key].parent.split('_')[0] === currrentKey.split('_')[0]) {
        dispatch(removeComponent(key));
      }
    });
  }

  componentWillMount () {
    let {dispatch, selectedComponents} = this.props;

    // checking page is rendering in editor or Renderer by tempFlag
    let tempFlag = false;
    for (let key in selectedComponents) {
      if (selectedComponents[key].final === true) {
        tempFlag = true;
        break;
      }
    }

    // Auto dropping components for this template when page is editor
    if (tempFlag === false) {
      var compData = {properties: {}};
      for (let key in ComponentsData['PageHeader'].properties) {
        compData.properties[key] = ComponentsData['PageHeader'].properties[key];
      }
      dispatch(dropComponent(
        'PageHeader',
        compData,
        this.constructor.displayName
      ));

      compData = {properties: {}};
      for (let key in ComponentsData['ProductPanelContainer'].properties) {
        compData.properties[key] = ComponentsData['ProductPanelContainer'].properties[key];
      }
      dispatch(dropComponent(
        'ProductPanelContainer',
        compData,
        this.constructor.displayName
      ));

      compData = {properties: {}};
      for (let key in ComponentsData['PageFooter'].properties) {
        compData.properties[key] = ComponentsData['PageFooter'].properties[key];
      }
      dispatch(dropComponent(
        'PageFooter',
        compData,
        this.constructor.displayName
      ));

      compData = {properties: {}};
      for (let key in ComponentsData['CarouselInstance'].properties) {
        compData.properties[key] = ComponentsData['CarouselInstance'].properties[key];
      }
      dispatch(dropComponent(
        'CarouselInstance',/*Carousel items were not getting added  after choosing homepagetemplate 
        ->Carousel changed to CarouselInstance */
        compData,
        this.constructor.displayName
      ));

      compData = {properties: {}};
      for (let key in ComponentsData['NavBar'].properties) {
        compData.properties[key] = ComponentsData['NavBar'].properties[key];
      }
      dispatch(dropComponent(
        'NavBar',
        compData,
        this.constructor.displayName
      ));

    //  dispatch(setEdit());
    }
  }

  render () {
    let { selectedComponents, dispatch } = this.props;
    
    // variables to hold components
    let PageHeader = '';
    let PageFooter = '';
    let ProductPanelLayout = '';
    let Carousel = '';
    let NavBar = '';

    // Loop for rendering components from state
    Object.keys(selectedComponents).forEach((key) => {
      if (selectedComponents[key].parent === 'HomePageTemplate') {
        final = selectedComponents[key].final;
        console.log('final :', selectedComponents[key].final);
        let compName = key.split('_')[0];
        let Component = components[compName];
        let currrentKey = key;
        let CompWithProps = '';
        if (selectedComponents[key].final === false) {
          CompWithProps = (
            <EditableWrapper
            key={key.split('_')[1]}
            onClick={(ev) => { ev.stopPropagation(); dispatch(setEdit(currrentKey)); }}
            onDelete={() => dispatch(removeComponent(currrentKey))}
            isEditable={selectedComponents[key].edit}
            template={true} >
              <Component {...selectedComponents[key].data.properties} 
              selectedComponents={selectedComponents}
              dispatch={dispatch}/>
            </EditableWrapper>
          );
        } else {
          CompWithProps = (
            <Component {...selectedComponents[key].data.properties}
            selectedComponents={selectedComponents}
            dispatch={dispatch}/>
          );
        }

        if (compName === 'PageHeader') {
          PageHeader = CompWithProps;
        } else if (compName === 'ProductPanelContainer') {
          if (selectedComponents[key].final === false) {
            ProductPanelLayout = (
              <EditableWrapper
              key={key.split('_')[1]}
              onClick={(ev) => { ev.stopPropagation(); dispatch(setEdit(currrentKey)); }}
              onDelete={() => dispatch(removeComponent(currrentKey))}
              isEditable={selectedComponents[key].edit}
              template={true} >
                <DroppableWrapper>
                  <Component {...selectedComponents[key].data.properties}/>
                </DroppableWrapper>
              </EditableWrapper>
            );
          } else {
            ProductPanelLayout = (<Component {...selectedComponents[key].data.properties} />);
          }
        } else if (compName === 'CarouselInstance') {
          Carousel = CompWithProps;
        } else if (compName === 'PageFooter') {
          PageFooter = CompWithProps;
        } else if (compName === 'NavBar') {
          NavBar = CompWithProps;
        }
      }
    });

    return (
      <div>
        <div id='PageHeader'>
          {PageHeader}
        </div>
        <div id='NavBar' style={{border: final ? 'none' : '1px solid #DAD4D4'}}>
          {NavBar}
        </div>
        <div id='Carousel' style={{border: final ? 'none' : '1px solid #DAD4D4'}}>
          {Carousel}
        </div>
        <div id='ProductPanelLayout'>
          {ProductPanelLayout}
        </div>
        <div id='PageFooter'>
          {PageFooter}
        </div>
      </div>
    );
  }
}

export default HomePageTemplate
