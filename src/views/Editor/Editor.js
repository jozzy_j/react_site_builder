import React, { PropTypes } from 'react';
import classes from './Editor.scss';
import { connect } from 'react-redux';
import EditProperty from 'components/EditProperty';
import ToolBox from 'components/ToolBox';
import Canvas from 'components/Canvas';
import headerImg from 'img/headerImg.jpg';
import { Link } from 'react-router'
import { Grid, Row, Col } from 'react-bootstrap';
import back from '../../img/back.png';
import {fetchPosts, receivePosts, onChangingPageUrl} from '../../redux/actions';

export class Editor extends React.Component {
  constructor (props) {
    super(props);
  }
  static propTypes = {
    selectedComponents: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };
  componentDidMount () {
    const {pageUrl, selectedComponents, location, dispatch} = this.props;
    if(location.query.id!= undefined){    
      dispatch(fetchPosts('id',location.query.id));
      dispatch(onChangingPageUrl(location.query.url));
    }
  }
  render () {
    const {selectedComponents,pageUrl,dispatch} = this.props;
    return (
      <div >
        <div id={classes.headerTop} className='clearfix' >
        <div id={classes.branding} className='clearfix'>
          <img id={classes.imageStyle} src={headerImg} alt='SIGMA-ALDRICH LOGO' />

        </div>
         <Link to='/' style={{float: 'right', marginRight:'35'}}> <img style={{height:'70',marginTop:'15'}} src={back} alt='Go to Home Page' /></Link>  
      </div>
            <div className={classes.toolBox}>
              <ToolBox/>
            </div> 
            <div className={classes.editor}>
              <Canvas {...this.props}/>
            </div>
            <div className={classes.editProperty}>
              <EditProperty currentComponent={selectedComponents} dispatch= {dispatch}/>
            </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  selectedComponents: state.page.selectedComponents,
  pageUrl:state.page.pageUrl,
  showModal:state.showModal
});
export default connect(mapStateToProps)(Editor);

// <EditProperty selectedComponents={selectedComponents}/>