import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import {fetchPosts, receivePosts } from '../../redux/actions';
import * as ComponentCollection from 'allComponents/ComponentCollection.js';

export class CustomPageView extends React.Component {
  componentDidMount () {
    const {pageUrl, selectedComponents, location, dispatch} = this.props;
    let path = location['pathname'];
    dispatch(fetchPosts('pageUrl',path));
  }

  render () {
    let {dispatch,selectedComponents} = this.props;
    let components = selectedComponents;
    let getComponents = [];
    for (var key in components) {
      if (components[key].parent === '') {
        let final = components[key].final;
        let compName = key.split('_')[0];
        let Component = ComponentCollection[compName];
        let currrentKey = key;
        
        getComponents.push(
          <Component 
            key={key.split('_')[1]} {...components[key].data.properties}
            selectedComponents={selectedComponents}
            dispatch={dispatch}
            final={final}/>
        );
      }
    }
    return (
        <div>
          {getComponents}
        </div>
    );
  }
}

const mapStateToProps = (state) => ({
  selectedComponents: state.page.selectedComponents
});
export default connect(mapStateToProps)(CustomPageView);