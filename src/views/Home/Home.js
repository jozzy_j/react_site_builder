import React from 'react';
import { Link } from 'react-router';
import {Table} from 'components/Table';
import {GenericWrapper} from 'containers/GenericWrapper';
import classes from 'views/Editor/Editor.scss';
import headerImg from 'img/headerImg.jpg';

export class Home extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      searchKeyword: '',
      PageList: [],
      taskCompleted: ''
    };
  }
  removeItem (taskCompleted, taskName) {
    let tasks = this.state.products;
    tasks.map((toDo, i) => {
      if (toDo.task.indexOf(taskName) > -1) {
        tasks.splice(i, 1);
      }
    });
    this.setState({products: tasks});
  }
  searchList (searchKeyword) {
    this.setState({searchKeyword: searchKeyword});
  }
  componentDidMount () {
    // This doesn't refer to the `span`s! It refers to the children between
    // last line's `<App></App>`, which are undefined.
    fetch('http://localhost:1337/name/PageList')
    .then((response) => {
      return response.json();
    }).then((json) => {
      this.setState({PageList: json});
    }).catch((ex) => {
      console.log('parsing failed', ex);
    });
  }
  render () {
    return (
      <div >
        <div id={classes.headerTop} className='clearfix' >
          <div id={classes.branding} className='clearfix'>
            <img id={classes.imageStyle} src={headerImg} alt='Page Maker LOGO' />
          </div>
        </div>
        <div style={{overflowY: 'scroll', backgroundColor: 'rgba(28, 210, 82, 0.09)', height: '699'}}>
          <GenericWrapper>
            <Link to='/designer' style={{float: 'right'}}><h3>Create New Page</h3></Link>
            {
              this.state.PageList.length === 0
              ? <div style={{color: '#DDDDDD', marginLeft: 'auto', marginRight: 'auto'}}>
                <h1 style={{fontSize: '120'}} className='text-center'>Welcome To The Page Maker</h1>
              </div>
              : <div >
                <div style={{color: '#DDDDDD', marginRight: '170', zIndex: '-1', position: 'fixed'}}>
                  <h1 style={{fontSize: '140'}} className='text-center'>Welcome To The Page Maker</h1>
                </div>
                <Table PRODUCTS={this.state.PageList} searchKeyword={this.state.searchKeyword}/>
              </div>
            }
          </GenericWrapper>
        </div>
      </div>
    );
  }
}

export default Home;
