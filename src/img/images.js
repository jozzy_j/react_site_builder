import city from 'img/city.jpg';
import defaultImage from 'img/defaultImage.png';
import sigmaImg from 'img/sigmaImg.jpg';
import express from 'img/express.jpg';
import sigmaNew from 'img/sigmaNew.jpg';
import sigmaImg3 from 'img/sigmaImg3.jpg';
import advanceScience from 'img/advanceScience.jpg';
import takinScience from 'img/takinScience.jpg';
import alternative from 'img/alternative.jpg';
import expand from 'img/expand.jpg';
import certifiedFoods from 'img/certifiedFoods.png';
import Contact from 'img/Contact.png';
import editIcon from 'img/editIcon.png';
import Mail from 'img/Mail.png';
import OrderCenter from 'img/OrderCenter.png';
import CarouselIcon from 'img/CarouselIcon.png';
import headerImg from 'img/headerImg.jpg';
import sigma1 from 'img/sigma1.jpg';
export default {
  city: city,
  defaultImage: defaultImage,
  sigmaImg: sigmaImg,
  express: express,
  sigmaNew: sigmaNew,
  sigmaImg3: sigmaImg3,
  certifiedFoods: certifiedFoods,
  Contact: Contact,
  editIcon: editIcon,
  Mail: Mail,
  OrderCenter: OrderCenter,
  expand: expand,
  advanceScience: advanceScience,
  takinScience: takinScience,
  alternative: alternative,
  CarouselIcon:CarouselIcon,
  headerImg:headerImg,
  sigma1
};
