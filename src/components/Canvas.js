import React, {PropTypes} from 'react';
import {Button,Modal} from 'react-bootstrap';
import ComponentsData from 'allComponents/ComponentsData';
import TextBox from 'components/TextBox';
import Panel from 'components/Panel';
import ModalPopUp from 'components/ModalPopUp';
import * as components from 'allComponents/ComponentCollection';

import {dropComponent, setEdit,sendPosts,openModal,updatePage} from '../redux/actions';

export class Canvas extends React.Component {
  constructor (props) {
    super(props);
  }
  static propTypes = {
    selectedComponents: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    pageUrl: PropTypes.string.isRequired
  };
drop =(ev) => {
      ev.preventDefault();
      let flag=true;
      var compName = ev.dataTransfer.getData('name');
      
      const {dispatch,selectedComponents} = this.props;

    //checks component is droppable here or not
    Object.keys(components).forEach((key) => {
        if(key == compName) {
          flag = true;
        }
    });

    //checks component is already in state or not
    Object.keys(selectedComponents).forEach((key) => {
      let keyName = key.split("_");
      if((keyName[0].indexOf('Layout') > -1 && compName.indexOf('Layout') > -1)
        || (keyName[0].indexOf('Template') > -1 && compName.indexOf('Template') > -1)
         || (keyName[0].indexOf('PageHeader') > -1 && compName.indexOf('PageHeader') > -1)
          || (keyName[0].indexOf('PageFooter') > -1 && compName.indexOf('PageFooter') > -1)) {
        console.log('dsfdfdfdfdfdf', key);
        flag=false;
      }
    });

    if (flag===true) {
    var compData = {};
    //copying components properties
      if (compName !== 'GridLayout') {
        compData = {properties:{}, initialState:{}};
      } else {
        compData = {properties: {}, initialState:{}, layouts: {}};
      }
      for (var key in ComponentsData[compName].properties) {
        compData.properties[key]=ComponentsData[compName].properties[key];
      }
        //var compData = Object.assign({},ComponentsData[compName]);
      dispatch(setEdit());
      dispatch(dropComponent(compName,compData));
    } else { 
      dispatch(openModal());
    }


}
  allowDrop = (ev) => {
    ev.preventDefault();
  }
  saveChanges = (e) => {
    const {selectedComponents, pageUrl, dispatch} = this.props;
    dispatch(setEdit());
    if(pageUrl!='' && Object.keys(selectedComponents).length != 0){
     dispatch(sendPosts(pageUrl,selectedComponents));  
    }
     else{
       alert("Please add page url properly or Drop component");
    }
  }
  UpdatePage = (e) => {
    const {selectedComponents, location,pageUrl, dispatch} = this.props;
    dispatch(setEdit());
    if(pageUrl!='' && Object.keys(selectedComponents).length != 0){
     dispatch(updatePage(location.query.id,pageUrl,selectedComponents));  
    }
    else{
      alert("Please add page url properly or Drop component");
    }
  }
  render () {
  	let { pageUrl,dispatch,selectedComponents,showModal,location,modalBody} = this.props;

    let buttonToShow=location.query.id?
      <Button ref='Update' 
        bsStyle="primary"
        onClick={(e) => this.UpdatePage(e)}>
        Update
      </Button>:
      <Button style={{float:'right',marginTop:'13',marginRight:'20'}} ref='SaveChanges' 
        bsStyle='primary'
        onClick={this.saveChanges}>
        Save Changes
      </Button>;
    return (
      <div>
      <ModalPopUp modalTitle='Warning!' modalBody='This item cannot be used twice,Please choose another item' showModal={showModal}
        dispatch={dispatch}/>
            <TextBox pageUrl={pageUrl} URL='URL' dispatch= {dispatch} style={{margin:'2px'}}/>
            <Panel
            panelTitle='Main Contents'
            componentscollection={selectedComponents}
            dropComponent={this.drop}
            allowDropcomponent={this.allowDrop}
            dispatch= {dispatch}
            style={{margin:'2px'}}/>
            {buttonToShow}
      </div>
    );
  }
}

export default Canvas;