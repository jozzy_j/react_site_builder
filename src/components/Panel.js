import React, {PropTypes} from 'react';
import * as ComponentCollection from 'allComponents/ComponentCollection.js';
import {setEdit, removeComponent} from 'redux/actions';
import EditableWrapper from 'containers/wrappers/EditableWrapper';
import DroppableWrapper from 'containers/wrappers/DroppableWrapper';

export class Panel extends React.Component {
  static propTypes = {
    panelTitle: PropTypes.string.isRequired,
    dropComponent: PropTypes.func.isRequired,
    allowDropcomponent: PropTypes.func.isRequired,
    componentscollection: PropTypes.object.isRequired,
    style: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  }
  addPanelItem () {
    console.log('add clicked');
  }
  handleDelete = (currrentKey) => {
    let { componentscollection, dispatch } = this.props;
    Object.keys(componentscollection).forEach((key) => {
      if (componentscollection[key].parent.split('_')[0] === currrentKey.split('_')[0]) {
        dispatch(removeComponent(key));
      }
    });
  }
  render () {
    let {dispatch, componentscollection, style} = this.props;
    let components = this.props.componentscollection;
    let getComponents = [];
    for (var key in components) {
      let final = components[key].final;
      console.log("inside panel ",final);
      let compName = key.split('_')[0];
      let Component = ComponentCollection[compName];
      let currrentKey = key;
      if (components[key].parent === '') {
        getComponents.push(
          <EditableWrapper
            key={key.split('_')[1]}
            onClick={() => { dispatch(setEdit(currrentKey)); }}
            onDelete={() => {
              dispatch(removeComponent(currrentKey));
              this.handleDelete(currrentKey);
            }}
            isEditable={components[key].edit}>
            <DroppableWrapper>
              <Component {...components[key].data.properties}
                selectedComponents={componentscollection}
                dispatch={dispatch}
                final={final}/>
            </DroppableWrapper>
          </EditableWrapper>
        );
      }
    }
      return (
        <div className='panel panel-default' style={style}>
          <div className='panel-heading'>
            <h3 className='panel-title text-center'>{this.props.panelTitle}</h3>
          </div>
          <div 
          className='panel-body' 
          onDrop={this.props.dropComponent} 
          onDragOver={this.props.allowDropcomponent}
          style={{overflowY: 'scroll', height: '550px'}}>
            {Object.keys(componentscollection).length === 0
              ? <div style={{color: '#DDDDDD', marginLeft: 'auto', marginRight: 'auto'}}>
                <h1 style={{fontSize:'40'}} className='text-center'>DROP COMPONENTS HERE</h1>
                </div>
              : getComponents}
          </div>
        </div>
      );
  }
}

export default Panel;
