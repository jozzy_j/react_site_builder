import React from 'react'

export class CustomComponent extends React.Component {
  render () {
    let InputType = this.props.inputType;
    return <InputType type ='text' ref = 'input'/>;
  }
}

export default CustomComponent
