import React, { PropTypes } from 'react';
//import 'styles/Accordion.scss';
import styles from 'styles/Accordion.scss';

export class Section extends React.Component{
  constructor (props) {
    super(props)
    this.state = {
      open: false,
      class: 'section'
    }
  }
  handleClick=()=>{
        if(this.state.open) {
      this.setState({
        open: false,
        class: "section"
      });
    }else{  
      this.setState({
        open: true,
        class: "sectionopen"
      });
    }
  }
  render(){
    return (
      <div className={styles[this.state.class]}>
        <button className={styles['button']}>toggle</button>
        <div className={styles['sectionhead']} onClick={this.handleClick}>{this.props.title}</div>
        <div className={styles['articlewrap']}>
          <div className={styles['article']}>
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

export class Accordion extends React.Component{
  render(){
        return (
      <div className={styles['main']}>
        <div className={styles['title']}>{this.props.title}</div>
        {this.props.children}
      </div>
    );
  }
}