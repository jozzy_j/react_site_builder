import React, {PropTypes} from 'react';
import {ItemRow} from 'components/LinkItemRow';
export class Table extends React.Component {
  static propTypes = {
    PRODUCTS: PropTypes.array.isRequired
  };
  render () {
    let data =this.props.PRODUCTS;
    let sections = [];
    data.map((product, i) => {
      sections.push(<ItemRow PRODUCT={product} searchKey={this.props.searchKeyword}
        taskDone={this.props.onRemoval}/>);
    });
    return (
      <table className='table ' >
        <colgroup>
          <col width='30%' />
          <col width='20%' />
          <col width='50%' />
        </colgroup>
        <thead>
          <tr>
            <th>Page Url</th>
          </tr>
        </thead>
        <tbody>{sections}</tbody>
      </table>
  );
  }
}
