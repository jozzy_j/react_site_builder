import React from 'react';
import classes from 'styles/PageHeader.scss';
import sigmaImg from 'img/sigmaImg.jpg';
import images from 'img/images';
export default class PageHeader extends React.Component {
  render () {
  	let {file}=this.props;
    if((Object.keys(this.props).length)==0){
       return (
      <div id={classes.headerTop} className='clearfix' >
        <div id={classes.branding} className='clearfix'>
          <img id={classes.imgStyle} src={sigmaImg} alt='SIGMA-ALDRICH LOGO' />
        </div>
      </div>
    );
    }
   
  	else if(file=='sigmaImg'){
      let image = images[file];
  		 return (
      <div id={classes.headerTop} className='clearfix' >
        <div id={classes.branding} className='clearfix'>
          <img id={classes.imgStyle} src={image} alt='SIGMA-ALDRICH LOGO' />
        </div>
      </div>
    );

  	}
  	else{
    let src=file.split(/\\/);
    var src1=src[2];
    let src2=src1.split(/\./);
    let src3=src2[0];
    let image = images[src3];
    return (
      <div id={classes.headerTop} className='clearfix' >
        <div id={classes.branding} className='clearfix'>
          <img id={classes.imgStyle} src={image} alt='SIGMA-ALDRICH LOGO' />
        </div>
      </div>
    );
  	}
   
  }
}


