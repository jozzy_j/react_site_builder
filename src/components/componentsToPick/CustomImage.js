import React from 'react';
import {Image} from 'react-bootstrap';
import images from 'img/images';

export default class CustomImage extends React.Component {
  static propTypes = {
    file: React.PropTypes.string,
    name: React.PropTypes.string,
    linkValue: React.PropTypes.string
  }
  render () {
    let {file, name, linkValue} = this.props;
    let imageSrc = '';
    if (file === 'defaultImage' || file === '' ) {
      imageSrc = images[file];
    }
    else {
      let src = file.split(/\\/);
      let src1 = src[2];
      let src2 = src1.split(/\./);
      let src3 = src2[0];
      imageSrc = images[src3];
    }
    return (
      <div style={{position: 'relative', marginLeft: 'auto', marginRight: 'auto'}}>
        <a href={linkValue}>
          <Image src={imageSrc} alt={linkValue} name={name} responsive />
        </a>
      </div>
    );
  }
}
