import React from 'react';

export default class customLink extends React.Component{
  static PropTypes = {
    linkValue:React.PropTypes.string,
    data:React.PropTypes.Array
  }
  render () {
  	let {text,linkValue}=this.props;

    return (
         <li>
           <a href = {linkValue} onClick= {(e) => e.stopPropagation}> {text} </a>
          </li> 
     )
    }
} 
