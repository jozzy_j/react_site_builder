import React from 'react';
import {Panel} from 'react-bootstrap';
import classes from 'styles/PageHeader.scss';

var footerStyle = {
  bottom: '0',
  position: 'fixed',
  width: '100%'
};

export default class PageFooter extends React.Component {
  static propTypes = {
    text: React.PropTypes.string,
    onClick: React.PropTypes.func,
    selectedComponents: React.PropTypes.object
  };

  render () {
    let {text} = this.props;
    /*let finalFlag = false;
    for (let key in selectedComponents) {
      if (selectedComponents[key].final === true) {
        finalFlag = true;
        break;
      }
    }*/
    return (
      <footer>
        <Panel id={classes.headerTop} className='clearfix'>
          <p className='text-center bold'> {text} </p>
        </Panel>
      </footer>
    );
  }
}
