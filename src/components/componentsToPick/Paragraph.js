import React from 'react';

export default class Paragraph extends React.Component {
  static propTypes = {
    content: React.PropTypes.string
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <span>
      	{this.props.content}
      </span>
    );
  }
}
