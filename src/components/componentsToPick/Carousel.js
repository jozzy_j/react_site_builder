import React, {PropTypes} from 'react';
import {Carousel,CarouselItem} from 'react-bootstrap';
import images from 'img/images';

var imageStyle = {
    position: 'relative',
    maxWidth: '100%',
    maxHeight: '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
}
export class CarouselInstance extends React.Component {
static propTypes = {
    items: PropTypes.array.isRequired,
  };
  render () {
    let { style, grid} = this.props;
      let carouselItems=this.props.items;
      let carousel=[];
      for (let index in carouselItems) {
        let currentItem=carouselItems[index];
            carousel.push(
        <CarouselItem key={index}>
          <img alt="300x500" src={images[currentItem.file]} style={imageStyle}/>
          <div className="carousel-caption">
            <h3>{currentItem.contentTitle}</h3>
            <p>{currentItem.content}</p>
          </div>
        </CarouselItem>)
      }
    return (
      <div>
        <Carousel style={{minHeight: grid ? 'none' : '300px', position: grid ? 'absolute' : 'none', width: grid ? '100%' : 'none'}}>
          {carousel}
        </Carousel>
      </div>
    );
  }
 }

export default CarouselInstance;