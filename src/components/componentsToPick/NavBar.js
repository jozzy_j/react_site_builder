import React, {PropTypes} from 'react';
import {Nav, NavItem, MenuItem, NavDropdown, Navbar} from 'react-bootstrap';
export class NavBar extends React.Component {
  static propTypes = {
    listItems: PropTypes.array.isRequired,
  };
  handleSelect=(ev,key) => {
  }
  render () {
    let {template} = this.props;
      let navBarItems=this.props.listItems;
      let navBar=[];

      for(let i=0,length=navBarItems.length;i<length;i++){
        let title=navBarItems[i]['title'];
            let menuItems=[];

        if (navBarItems[i].hasOwnProperty('options')) {
            let options=navBarItems[i]['options'];
            for (var option in options){             
              menuItems.push(<MenuItem key={option} eventKey={i+'.'+option}>{options[option]}</MenuItem>);
            }
        }
            navBar.push(<NavDropdown key={title+'_'+i} eventKey={i} title={title} id='nav-dropdown'>{menuItems}</NavDropdown>)

      }

      
    return (
      <div style={{minHeight: '30px'}}>
        <Nav bsStyle="pills" activeKey={1} onSelect={this.handleSelect}>
          {navBar}
      </Nav>
      </div>
    );
  }
}

export default NavBar;