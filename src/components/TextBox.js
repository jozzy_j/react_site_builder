import React, {PropTypes} from 'react';
import {onChangingPageUrl} from '../redux/actions';

export class TextBox extends React.Component {
  constructor (props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  static propTypes = {
    style: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    pageUrl: PropTypes.string.isRequired
  };
  handleChange () {
    const {dispatch} = this.props;
    dispatch(onChangingPageUrl(this.refs.URL.value));
  }
  render () {
    let {style} = this.props;
    return (
      <div className='form-group' style={style}>
        <input type='text' className='form-control' placeholder='Type the url for the new page'
          ref={this.props.URL} onChange={this.handleChange} value={this.props.pageUrl}></input>
      </div>
    );
  }
}

export default TextBox;
