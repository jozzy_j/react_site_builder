import React, {PropTypes} from 'react';
import {Modal, Button} from 'react-bootstrap';
import {closeModal} from '../redux/actions';

export class ModalPopUp extends React.Component {
  static propTypes = {
    modalTitle: PropTypes.string.isRequired,
    modalBody: PropTypes.string.isRequired,
    showModal: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired
  };
  close= () => {
    const {dispatch} = this.props;
    dispatch(closeModal());
  }
  render () {
    console.log(this.props.modalBody,this.props.showModal);
    return (
      <div className='static-modal'>
        <Modal show={this.props.showModal}>
          <Modal.Header>
            <Modal.Title>{this.props.modalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.props.modalBody}
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalPopUp;
