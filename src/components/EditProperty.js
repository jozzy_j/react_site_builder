import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import * as ComponentCollection from 'allComponents/ComponentCollection.js';
import * as PropertyCollection from '../../config/config';
import {updateData, dropComponent, SetCurrentComponent, setProperties,setEdit,updateItem} from '../redux/actions';
import CustomComponent from 'components/CustomComponent';
import {Section,Accordion} from 'components/Accordian';
import {Input, Button, FormGroup, ControlLabel, FormControl} from 'react-bootstrap';

export class EditProperty extends React.Component {
	constructor (props) {
	 super(props);
     this.saveUpdate = this.saveUpdate.bind(this);
     this.getCurrentComponent = this.getCurrentComponent.bind(this);
  }

  saveUpdate (e) {
    const {dispatch,currentComponent} = this.props;
    let currentItemData=this.getCurrentComponent();
    let data=currentItemData.properties;
    let componentKey = currentItemData.componentKey;
    for(let key in data){
      console.log(this.refs[key].getValue());
      data[key]=this.refs[key].getValue();
    }
    //let componentKey = this.getCurrentComponent().componentKey;
    dispatch(setProperties(componentKey,{properties:data,initialState:currentItemData.initialState}));
  }
  getCurrentComponent () {
    const {currentComponent} = this.props;
    //contains component key and properties
    let currrentComponentObject= {};
      for(let key in currentComponent){        
        if(currentComponent[key].edit == true){
          if(typeof currentComponent[key].data.properties.initialState !='undefined')
           {
                let copy = {};          
            for(var index in currentComponent[key].data.properties.initialState)
            { 
            copy[index] = currentComponent[key].data.properties.initialState[index];
            }
            currrentComponentObject.properties =copy;
            currrentComponentObject.initialState=currentComponent[key].data.properties.initialState;
            currrentComponentObject.componentKey = key;
          }else{
             let copyy = {};          
            for(var index in currentComponent[key].data.properties)
            { 
            copyy[index] = currentComponent[key].data.properties[index];
            }
          currrentComponentObject.properties = copyy;
          currrentComponentObject.initialState=currentComponent[key].data.initialState;
          currrentComponentObject.componentKey = key;
          }
        }
      }
    return currrentComponentObject;
  }
  MenuSelected=(eventKey,ev,itemindex)=>{
     const {dispatch} = this.props;
    //dispatch(setEdit(compKey));
    console.log();
  }
    editItem=(compKey,componentKey)=>{
     const {dispatch, currentComponent} = this.props;
     let currentComponentDetails=this.getCurrentComponent();
            let data=currentComponentDetails.properties;
            let ind=0;
            let inState=currentComponentDetails.initialState;
            let index=compKey.split('_')[1];
            /*console.log(data);
     for(let key in data){
      console.log(key);
      console.log(compKey,this.refs);
      //data[key]=this.refs[key+'_'+ind].getValue();
      //this.refs[key+'_'+ind].refs.input.value='';
      ind++;*/
       let currentComp=currentComponent[componentKey];
       let updatedKey='';
       let optionsArray='';
      let itemsAlreadySelected=currentComp.data.properties.listItems;
        updatedKey=this.refs[compKey].getValue();
        for(let i=0,length=itemsAlreadySelected.length;i<length;i++){
          if(index==i){  
                        let title=itemsAlreadySelected[i]['title'];            
            if (itemsAlreadySelected[i].hasOwnProperty('options')) {
                let options=itemsAlreadySelected[i]['options'];
                  //if(options instanceof Array){
                      //for(let i=0,length=options.length;i<length;i++){
                        optionsArray=this.refs['options_'+ind].getValue();
                      //}
                  //}
                }
              }
      ind++;
      
            }
     if(updatedKey!=''){

       dispatch(updateItem(componentKey,index,updatedKey,optionsArray,inState));//(component,itemkey,updateditem key,updatedoptions,intial state)      
     }
  }
  render () {
    
    if(Object.keys(this.getCurrentComponent()).length===0){
      return(
        <Accordion ref='Accordion' title='Select Tool from Toolbox'></Accordion>
        )
    }
    else
    {
       let currentComponentDetails=this.getCurrentComponent();
            let properties=currentComponentDetails.properties;
            let componentKey=currentComponentDetails.componentKey;
            let componentDisplayName=componentKey.split('_')[0];
    const {currentComponent} = this.props;
       let currentComp=currentComponent[componentKey];
      let editData=[];
       let inputData=[];
        for(let key in properties){
        inputData.push(
           <Input type ={key} accept='image/*' ref = {key} key={key} placeholder={properties[key]} defaultValue={properties[key]}/>
          )
      }
      inputData.push(<Button bsStyle='default' onClick={(e) => this.saveUpdate(e)}>Update</Button>);
      switch(componentDisplayName){
        
        case 'CarouselInstance':
        componentDisplayName='Carousel';
        editData.push(inputData);
        break;
        case 'NavBar':
      editData.push(<Section title='Add New Navbar Item'>{inputData}</Section>);        
        let ind=0;
        let itemsAlreadySelected=currentComp.data.properties.listItems;
         for(let i=0,length=itemsAlreadySelected.length;i<length;i++){
            let title=itemsAlreadySelected[i]['title'];
            let itemToEdit=[];
            let itemindex='title'+'_'+ind;
            itemToEdit.push(<Input type ={'text'} accept='image/*' ref = {'title'+'_'+ind} key={'title'+'_'+ind} placeholder={title}
              defaultValue={title}/>);
            if (itemsAlreadySelected[i].hasOwnProperty('options')) {
                let options=itemsAlreadySelected[i]['options'];
                  if(options instanceof Array){
                      //for(let i=0,length=options.length;i<length;i++){
                        itemToEdit.push(
                          <Input type ='text' accept='image/*' ref = {'options_'+ind} key={'options'+'_'+ind+'_'+i}
                           placeholder={options} defaultValue={options}/>
                        );
                      //}
                  }
                }
                itemToEdit.push(<Button bsStyle='default' onClick={(e) => this.editItem(itemindex,componentKey)}>Update</Button>);
      ind++;
      editData.push(<Section title={title}>{itemToEdit}</Section>);
            }        
        break;
        case 'GridLayout':
        case 'HomePageTemplate':
        case 'InfoPageTemplate':
        break;
        case 'ProductPanelContainer':
        componentDisplayName='Tabs';//Set components names as shown in tool box
        break;
        case 'Header':
         let HeaderData=currentComp.data.properties;
         let fontSizeOptions=[];
         for(let key in properties){
        
         if(properties[key] instanceof Array){
           let selectedItem=currentComp.data.properties[key]?currentComp.data.properties[key]:' ';       
            fontSizeOptions=properties[key].map((fontSize,i)=>{
              if(fontSize==selectedItem){
                return <option value={fontSize} selected='selected'>{fontSize}</option>;
              }else{
                return <option value={fontSize}>{fontSize}</option>;                
              }
              
            });
            editData.push(<Input type ='select' ref = {key} key={key} placeholder={properties[key]} >{fontSizeOptions}</Input>);   
          }else{
           let selectedHeaderContent=currentComp.data.properties[key]?currentComp.data.properties[key]:properties[key];                   
            editData.push(
           <Input type ={key} accept='image/*' ref = {key} key={key} placeholder={properties[key]} defaultValue={selectedHeaderContent}/>
          )
          }
        }

      editData.push(<Button bsStyle='default' onClick={(e) => this.saveUpdate(e)}>Update</Button>);
            
        break;
        default:   
        editData.push(inputData);
        break;
      }
      return (
      <div>
       <div>        
        {
        currentComp?<Accordion ref='Accordion' title={'Edit ' + componentDisplayName}>{editData}</Accordion>:''
        }

      </div>
      </div>
    );
    
    
  }
}
}

export default EditProperty;
