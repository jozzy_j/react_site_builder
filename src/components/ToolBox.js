import React from 'react';
import {Button,ButtonGroup, Clearfix, MenuItem} from 'react-bootstrap';

class ToolBox extends React.Component {
  drag (ev) {
    ev.dataTransfer.setData('name', ev.target.id);
  }
  render () {
    
    return (
      <div>
      <p className='text-center bold'> <label>TOOL BOX</label></p>

      <ButtonGroup vertical block className='well' style={{maxWidth: 400, margin: '0 auto 10px'}}>
      <h5 className='text-center '>Templates</h5>

          <Button id='HomePageTemplate' draggable='true'onDragStart={this.drag} >
              <span key='HomePage' style={{cursor:'move'}} >Primary Template</span>
          </Button>          
          <Button id='InfoPageTemplate' draggable='true'onDragStart={this.drag} style={{cursor:'move'}}>
              Secondary Template
          </Button>
          <Button id='GridLayout' draggable='true'onDragStart={this.drag} style={{cursor:'move'}}>
              Custom Template
          </Button>
  </ButtonGroup>

       <ButtonGroup vertical block className='well'>
      <h5 className='text-center bold'>Components</h5>   

          <Button style={{cursor: 'move'}} id='PageHeader' draggable='true' onDragStart={this.drag}>
            Page Header
          </Button>
          <Button id='PageFooter' draggable='true'onDragStart={this.drag} style={{cursor:'move'}}>
            Page Footer
          </Button>
          <Button id='ItemDescription' draggable='true'onDragStart={this.drag} style={{cursor:'move'}}>
            Thumbnail
          </Button>
          <Button id='NavBar' draggable='true'onDragStart={this.drag} style={{cursor:'move'}}>
            Nav Bar
          </Button>          
          <Button id='CarouselInstance' draggable='true'onDragStart={this.drag} style={{cursor:'move'}}>
            Carousel
          </Button>
          <Button id='ProductPanelContainer' draggable='true' onDragStart={this.drag} style={{cursor:'move'}}>
            Tabs
          </Button>
           <Button id='CustomLink' draggable='true' onDragStart={this.drag} style={{cursor:'move'}}>
            Link
          </Button>
          <Button id='CustomImage' draggable='true'onDragStart={this.drag} style={{cursor:'move'}}>
            Image
          </Button>
          <Button id='Paragraph' draggable='true'onDragStart={this.drag} style={{cursor:'move'}}>
            Paragraph
          </Button>
          <Button id='Header' draggable='true'onDragStart={this.drag} style={{cursor:'move'}}>
            Header
          </Button>
  </ButtonGroup>
        
      </div>
    );
  }
}

export default ToolBox;
