import React, { PropTypes } from 'react';
import images from 'img/images';
import {Image} from 'react-bootstrap';

export class ItemRow extends React.Component {
  static propTypes = {
    PRODUCT: PropTypes.object.isRequired
  };
  render () {
    let LinkValue = this.props.PRODUCT._id;
    return (
      <tr>
        <td><a href={this.props.PRODUCT.pageUrl}>{this.props.PRODUCT.pageUrl}</a></td>
        <td>
          <a href={'http://localhost:3000/Designer?id=' + LinkValue + '&url=' + this.props.PRODUCT.pageUrl}>
            <Image src={images.editIcon} alt={this.props.PRODUCT.pageUrl} responsive style={{float: 'right'}}/>
          </a>
        </td>
      </tr>);
  }
}
